<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/customers/{num?}', function ($num='No customer was provided') {
    if($num=='No customer was provided')
    return view('nocustomer',['num'=>$num]);
    else
    return view('customer',['num'=>$num]);
})->name('customers');

